<?php

namespace Drupal\ajax_requirement\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * Checks if passed parameter matches the route configuration.
 */
class IsAjaxRequestAccessChecker implements AccessInterface {

  /**
   * Access callback.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, Request $request) {
    $required_status = filter_var($route->getRequirement('_is_ajax_request'), FILTER_VALIDATE_BOOLEAN);
    $actual_status = $request->isXmlHttpRequest();
    if ($actual_status !== $required_status) {
      return AccessResult::forbidden();
    }
    return AccessResult::allowed();
  }

}
