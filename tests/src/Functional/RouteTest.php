<?php

namespace Drupal\Tests\ajax_requirement\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test basic functionality.
 *
 * @group ajax_requirement
 */
class RouteTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['ajax_route_test'];

  /**
   * Test the route file.
   *
   * @dataProvider getHeaders
   */
  public function testRoutes($headers, $codes) {
    $this->drupalGet('/my-ajax-path', [], $headers);
    $this->assertSession()->statusCodeEquals($codes[0]);
    $this->drupalGet('/my-non-ajax-path');
    $this->assertSession()->statusCodeEquals($codes[1]);
  }

  /**
   * Test with actual ajax requests.
   */
  public function getHeaders() {
    return [
      [
        [],
        [403, 200],
      ],
      [
        [
          'X-Requested-With' => 'XMLHttpRequest',
        ],
        [200, 403],
      ],
    ];
  }

}
