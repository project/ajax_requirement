<?php

namespace Drupal\ajax_route_test\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Ajax route test routes.
 */
class AjaxController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
